#include <winsock2.h>
#include <iostream>
#include <string>
#include <cstring>
#include <map>
#include <thread>
#include <vector>
#include <locale>
#include <shared_mutex>
#include <mutex>

class MyException : public std::exception
{
public:
	typedef std::basic_string<wchar_t> wstring;
	MyException(size_t i):err_num_(i){}
	inline size_t GetErrorNum()	{return err_num_;}
	private:
		size_t err_num_ = 0;
};
class WSAErr : public MyException
{
public:
	WSAErr(size_t i) : MyException(i) {}
};

class BindErr : public MyException
{
public:
	BindErr(size_t i) : MyException(i) {}
};

class ListenErr : public MyException
{
public:
	ListenErr(size_t i) : MyException(i) {}
};
class SocketErr : public MyException
{
public:
	SocketErr(size_t i) : MyException(i) {}
};

class MyServ
{
public:
	MyServ();
	~MyServ();


	typedef std::basic_string<wchar_t> wstring;




	void RunServer();

	static void WSAInit();
	static void WSADeinit();


private:
	SOCKET server_socket;
	std::shared_mutex mutex_;//muteble
	std::map<SOCKET, wstring> clients_;
	static const size_t mess_size_ = 512;
	static const size_t name_size_ = 32;

	wstring hello_message_;
	void CloseServSock();
	void Reading(const size_t& pinfo);
	void SetName(const SOCKET& sock, wchar_t name[]);
	void UserConnectedMessage(const SOCKET& sock, wchar_t name[]);
	void SendListUser(const SOCKET& sock);
	void EraseSock(const SOCKET& sock);

	void CreateandUseSocket(SOCKET& server_socket, sockaddr_in& saddr);
	void AcceptNewClients(const SOCKET& server_socket, const  sockaddr_in& saddr);
	

};

