// normchat.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "myserv.h"
#include "exception"
#include <string>



int main()
{
	try
	{
		MyServ::WSAInit();
	}
	catch (WSAErr& e)
	{
		std::wcout << L"WSAStartup failed with error :" << e.GetErrorNum()<< std::endl;
		exit(-1);
	}


	try
	{
			MyServ b;
			b.RunServer();
	}
	catch (SocketErr& e)
	{
		std::wcout << L"Creating socket error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (BindErr& e)
	{
		std::wcout << L"Binding error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (ListenErr& e)
	{
		std::wcout << L"Listening error with number:" << e.GetErrorNum() << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << "\n";
	}


	//std::vector <std::thread> th_vec;
	//try
	//{
	//	MyServ a;	
	////	auto thr = std::thread(&MyServ::RunServer, &a);
	//	th_vec.push_back(std::thread([&a]() { a.RunServer(); }));
	////	th_vec.push_back(std::thread(&MyServ::RunServer, std::ref(a)));
	//}
	//catch (const std::exception& e)
	//{
	//	std::cout << e.what() << "\n";
	//}


	//try
	//{
	//	MyServ b;
	//	b.RunServer();
	//}
	//catch (const std::exception& e)
	//{
	//	std::cout << e.what() << "\n";
	//}
	//for (auto& t : th_vec)
	//	t.join();

	MyServ::WSADeinit();


    return 0;
}