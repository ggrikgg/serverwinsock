#include "stdafx.h"

#include "MyServ.h"



MyServ::MyServ()
{

}


MyServ::~MyServ()
{

}

void MyServ::WSAInit()
{
	size_t err;
	WSADATA ws;
	err=WSAStartup(MAKEWORD(2, 0), &ws);

	if (err != 0) 
	{
		throw WSAErr(err);
	}
	//return;
}
void MyServ::WSADeinit()
{
	WSACleanup();
}

void MyServ::CreateandUseSocket(SOCKET& server_socket, sockaddr_in& saddr)
{
	if (server_socket == INVALID_SOCKET)
	{
		closesocket(server_socket);
		throw SocketErr(WSAGetLastError());
	}

	memset(&saddr, 0, sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(atoi("5000"));
	saddr.sin_addr.s_addr = INADDR_ANY;

	if (bind(server_socket, (sockaddr*)&saddr, sizeof(saddr)) == SOCKET_ERROR)
	{
		closesocket(server_socket);
		throw SocketErr(WSAGetLastError());
	}
	if (listen(server_socket, 1) == SOCKET_ERROR)
	{
		closesocket(server_socket);
		throw SocketErr(WSAGetLastError());
	}
}

void MyServ::CloseServSock()
{
	shutdown(server_socket,0);
	closesocket(server_socket);
}

void MyServ::AcceptNewClients(const SOCKET& server_socket,const  sockaddr_in& saddr)
{
	std::vector <std::thread> th_vec;
	int len = sizeof(saddr);
	while (true)
	{
		SOCKET client_socket = accept(server_socket, (sockaddr*)&saddr, &len);
		if (client_socket == INVALID_SOCKET)
		{   ///��� ������????????
			wprintf(L"����� (accept) ���������� ������� � �������: %ld\n", WSAGetLastError());
			closesocket(client_socket);
		}
		else
		{
			th_vec.push_back(std::thread(&MyServ::Reading, this, client_socket));
		}
	}

	for (auto& t : th_vec)
		t.join();
	closesocket(server_socket);
}

void MyServ::RunServer()
{
	server_socket = socket(AF_INET, SOCK_STREAM, NULL);
	sockaddr_in saddr;

	CreateandUseSocket(server_socket,saddr);

	std::wcout << L"Waiting for clients..." << std::endl;

	AcceptNewClients(server_socket, saddr);
}

void MyServ::SetName(const SOCKET& sock,wchar_t name[])
{
	std::unique_lock<std::shared_mutex> lock(mutex_);
//	clients_.insert(std::pair<SOCKET, wstring>(sock, name));
	clients_[sock] = name;

}
void MyServ::UserConnectedMessage(const SOCKET& sock, wchar_t name[])
{
	hello_message_ = L"����������� ������������: ";
	hello_message_ += name;
	hello_message_ += L'\n';

	std::shared_lock<std::shared_mutex> lock(mutex_);
	for (auto it = clients_.begin(); it != clients_.end(); ++it)
	{
		if (it->first != sock)
			send(it->first, (char*)hello_message_.c_str(), hello_message_.size() * sizeof(wchar_t), 0);
	}
}
void MyServ::SendListUser(const SOCKET& sock)
{
	wstring list_user;
	list_user = L"������ ���������� ����:\n";

	std::shared_lock<std::shared_mutex> lock(mutex_);
	for (auto it = clients_.begin(); it != clients_.end(); ++it)
	{
		list_user += it->second;
		list_user += L"\n";
	}
	send(sock, (char*)list_user.c_str(), list_user.size() * sizeof(wchar_t), 0);
}
void MyServ::EraseSock(const SOCKET& sock)
{
	std::unique_lock<std::shared_mutex> lock(mutex_);
	clients_.erase(sock);
	closesocket(sock);
}

void MyServ::Reading(const size_t& pinfo)
{
	std::cout << "new_connection!";
	SOCKET sock = pinfo;

	wchar_t name[name_size_];
	wchar_t messchar[mess_size_];

	if (recv(sock, (char*)name, name_size_ * sizeof(wchar_t), 0) == -1)
	{
		closesocket(sock);
		ExitThread(1);
	}

	SetName(sock, name);
	UserConnectedMessage(sock, name);
	SendListUser(sock);

	size_t it;

	wstring name_second_client;
	wstring private_message;
	wstring public_message;
	while (true)
	{
		if (recv(sock, (char*)messchar, mess_size_ * sizeof(wchar_t), 0) == -1)
		{
			EraseSock(sock);
			ExitThread(1);
		}
		wstring mess_ = messchar;

		if (mess_[0]== L'#')
		{
			it = mess_.find(':');
			name_second_client = mess_.substr(1, it-1);

			private_message = clients_[sock];
			private_message += L" ����� ���: ";
			private_message += mess_.substr(it);
			private_message += L'\n';

			std::shared_lock<std::shared_mutex> lock(mutex_);
			for (auto iter = clients_.begin(); iter != clients_.end(); ++iter)
			{
				if (iter->second== name_second_client)
				{
					send(iter->first, (char*)private_message.c_str(), private_message.size() * sizeof(wchar_t), 0);
				}
			}
			std::shared_lock<std::shared_mutex> unlock(mutex_);
		}
		else
			if (mess_[0] == L'*')
			{
				if (mess_ == L"*exitclose")
				{
					public_message = L"Connection closed \n";
					std::shared_lock<std::shared_mutex> lock(mutex_);
					for (auto it = clients_.begin(); it != clients_.end(); ++it)
					{
						send(it->first, (char*)public_message.c_str(), public_message.size() * sizeof(wchar_t), 0);
					}
					std::shared_lock<std::shared_mutex> unlock(mutex_);
					EraseSock(sock);
					CloseServSock();
					ExitThread(1);
				}
			}
			else
			{
				public_message = clients_[sock];
				public_message += L": ";
				public_message += mess_;
				public_message += L'\n';

				std::shared_lock<std::shared_mutex> lock(mutex_);
				for (auto it = clients_.begin(); it != clients_.end(); ++it)
				{
					if (it->first != sock)
						send(it->first, (char*)public_message.c_str(), public_message.size() * sizeof(wchar_t), 0);
				}
				std::shared_lock<std::shared_mutex> unlock(mutex_);
			}
		if (wcscmp(messchar, L"�����") == 0)
		{
			//	wcscpy(mess, L"��������� ���!");
			//	send(sock, (char*)mess, MessSize * sizeof(wchar_t), 0);
			EraseSock(sock);
			ExitThread(-2);
		}
	}
}